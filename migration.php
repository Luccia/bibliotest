<?php
// parcourir csv/

// pour chaque csv -> import et les modeles (Pour plus tard, on pourrait vérifier si table existe pour eviter doublon)

class Migration
{
    private $db;
    private $tableName;

    function __construct()
    {
        $this->db = new PDO('mysql:host=' .'localhost'. ';dbname=' . 'EnterNameDatabase','EnterUsername', 'EnterPassword',array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    }

    // ouvre /csv et liste les fichiers pour appeller getCsv()
    private function getFiles($directory)
    {
        if (is_dir($directory)) {
            if ($dh = opendir($directory)) {
                while (($file = readdir($dh)) !== false) {
                    if (is_file($directory . "/" . $file)) {
                        $this->tableName = (explode(".", $file))[0];
                        $this->getCsv($directory . "/" . $file);
                    }
                }
                closedir($dh);
            }
        }
    }

    // à partir d'un pointeur, récupère csv et appelle dbManagement()
    private function getCsv($fileHandle)
    {
        $file = fopen($fileHandle, 'r');
        while (!feof($file)) {
            $table[] = fgetcsv($file);
        }
        fclose($file);

        $this->dbManagement($table);
    }

    // à partir d'un tableau, procède à l'enregistrement en db
    // 1 - creer table
    // 2 - inserer données
    private function dbManagement(array $table)
    {

        $this->createTable(explode(';', $table[0][0]));
        $this->insertInDb($table);
    }

    // prend un tableau correspondnat aux colonnes
    private function createTable(array $columns)
    {
        $sql = "CREATE TABLE " . $this->tableName . "(id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,";

        //colonne4 type_donnees, -> , sauf dernier
        //A default de type on considère que tout est varchar(255) -> une option aurait été possible, réserver la 2eme ligne aux types
        foreach ($columns as $index => $column) {
            $sql .= str_replace(" ", "_", $column) . " VARCHAR(255)";
            // virgule ou pas?
            if ($index != count($columns)) {
                $sql .= ",";
            }
        }

        $sql .= "Etagere VARCHAR(255),Rangee INT)";
        $query = $this->db->prepare($sql);
        $query->execute();
    }

    // prend un tableau de tableaux correspondant aux entrées et la première entrée correspondant aux colonnes
    private function insertInDb(array $csv)
    {
        // si mon tableau a plus d'un index concatener dans l'ordre

        $sql = "INSERT INTO " . $this->tableName . " (";

        // je parcours mon csv
        for ($i = 0; $i < count($csv); $i++) {
            //pour chaque tableau je le parcours
            //foreach($csv as $value){       

            $value = implode('', $csv[$i]);

            $fieldsValues = str_getcsv($value, ';', '"');

            if ($i == 0) {
                foreach ($fieldsValues as $index => $column) {

                    $sql .= str_replace(" ", "_", $column);

                    if ($index != count($fieldsValues)) {
                        $sql .= ",";
                    }
                }
                $sql .= "Etagere,Rangee) VALUES (";
            } else {
                $tempSql = "";
                foreach ($fieldsValues as $index => $column) {
                    $tempSql .= "'" . htmlspecialchars($column, ENT_QUOTES) . "'";
                    if ($index != count($fieldsValues)) {
                        $tempSql .= ",";
                    }
                }
                $tempSql .= "'".$this->getRandomEtagere()."'".','.$this->getRandomRangee();
                //var_dump($i, count($csv)-1,$i < count($csv)-1 );
                $tempSql .= ")";
                echo $sql . $tempSql;
                $query = $this->db->prepare($sql . $tempSql);
                $query->execute();
            }
        }
    }

    public function getRandomEtagere(){
        $etagere = ["A","B","C","D","E","F","G","H"];
        $randomEtagere = array_rand($etagere, 1);
        return $etagere[$randomEtagere];
    }

    public function getRandomRangee(){
        $rangee = [];
        for($i = 0; $i < 7 ; $i++){
            $rangee[] = $i;
        }
        $randomRangee = array_rand($rangee, 1);

        return $randomRangee;
    }

    public function run()
    {
        $this->getFiles("csv");
    }
}

$migration = new Migration();
$migration->run();
