<?php
class Book extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('book_model');
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('pagination');
    }

    // // Affiche la liste des Books + recherche
    function index()
    {
        $config['base_url'] = 'http://localhost:8000/book/index/';
        $config['total_rows'] = $this->book_model->get_count();
        $config['per_page'] = 20;
        $config['uri_segment'] = 3;

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["links"] = $this->pagination->create_links();

        $keyword    =   $this->input->post('keyword');
        $data['results']    =   $this->book_model->search($keyword, $config['per_page'], $page);

        $this->load->view('books/list', $data);
    }

    public function show($id){
        $data['title'] = 'Books';
        $data['book'] = $this->book_model->get_book_by_id($id);
        $this->load->view('books/show', $data);

    }
    // Formulaire creation de Book
    public function create()
    {
        $data['title'] = 'Creer Livre';
        $data['fieldBook'] = $this->getFieldBook();
        $this->load->view('books/create', $data);
    }

    // Modifier un Book
    public function edit($id)
    {
        $id = $this->uri->segment(3);
        $data = array();

        if (empty($id)) {
            show_404();
        } else {
            $data['title'] = 'Editer Livre';
            $data['book'] = $this->book_model->get_book_by_id($id);
            $data['fieldBook'] = $this->getFieldBook();
            $this->load->view('books/edit', $data);
        }
    }

    public function getFieldBook()
    {

        $fieldBook = [
            "title" => "Titre",
            "name" => "Nom",
            "firstname" => "Prenom",
            "editor" => "Editeur",
            "book_format" => "Format",
            "type" => "Type",
            "section" => "Section",
            "shelf" => "Etagere",
            "row" => "Rangees"
        ];

        return $fieldBook;
    }

    // Ajoute un Book
    public function store()
    {

        $config = array(
            array(
                'field'   => 'title',
                'label'   => 'Titre',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'name',
                'label'   => 'Nom',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'firstname',
                'label'   => 'Prenom',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'editor',
                'label'   => 'Editeur',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'book_format',
                'label'   => 'Format',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'type',
                'label'   => 'Type',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'section',
                'label'   => 'Section',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'shelf',
                'label'   => 'Etagere',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'row',
                'label'   => 'Rangee',
                'rules'   => 'required'
            )
        );

        $this->form_validation->set_rules($config);

        $id = $this->input->post('id');

        if ($this->form_validation->run() === FALSE) {
            if (empty($id)) {
                redirect(base_url('book/create'));
            } else {
                redirect(base_url('book/edit/' . $id));
            }
        } else {
            $data['book'] = $this->book_model->createOrUpdate();
            redirect(base_url('book'));
        }
    }

    // Supprime un Book
    public function delete()
    {
        $id = $this->uri->segment(3);

        if (empty($id)) {
            show_404();
        }

        $this->book_model->delete($id);

        redirect(base_url('book'));
    }
}
