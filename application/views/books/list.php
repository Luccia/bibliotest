<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Bibliotech</title>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">
  <style>
    .mt40 {
      margin-top: 40px;
    }
  </style>
</head>

<body>
  <div class="container">
    <div class="row mt40">
      <div class="col-md-10">
        <h2>Bibliotech</h2>
      </div>
      <div class="col-md-2">
        <a href="<?php echo base_url('book/create/') ?>" class="btn btn-danger">Ajouter un livre</a>
      </div>
      <br><br>
      <form class="navbar-form" role="search" action="<?php echo base_url('book/index/') ?>" method="post">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Search" name="keyword" size="15px; ">
          <div class="input-group-btn">
            <button class="btn btn-default " type="submit" value="Search">Rechercher</button>
          </div>
        </div>
      </form>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Id</th>
            <th>Titre</th>
            <th>Nom</th>
            <th>Prénom</th>
            <th>Editeur</th>
            <th>Format</th>
            <th>Type</th>
            <th>Section</th>
            <th>Etagere</th>
            <th>Rangee</th>
            <td colspan="3">Action</td>
          </tr>
        </thead>
        <tbody>
          <?php if ($results) : ?>
            <?php foreach ($results as $result) : ?>
              <tr>
                <td><?php echo $result->id; ?></td>
                <td><?php echo $result->Titre; ?></td>
                <td><?php echo $result->Nom; ?></td>
                <td><?php echo $result->Prenom; ?></td>
                <td><?php echo $result->Editeur; ?></td>
                <td><?php echo $result->Format_livre; ?></td>
                <td><?php echo $result->Type; ?></td>
                <td><?php echo $result->Section; ?></td>
                <td><?php echo $result->Etagere; ?></td>
                <td><?php echo $result->Rangee; ?></td>
                <td><a href="<?php echo base_url('book/edit/' . $result->id) ?>" class="btn btn-primary">Modifier</a></td>
                <td>
                  <form action="<?php echo base_url('book/delete/' . $result->id) ?>" method="post">
                    <button class="btn btn-danger" type="submit">Supprimer</button>
                  </form>
                </td>
                <td>
                  <a href="<?php echo base_url('book/show/' . $result->id) ?>" class="btn btn-success">Voir</a>
                </td>
              <?php endforeach; ?>
            <?php endif; ?>
              </tr>
        </tbody>
      </table>
      <nav>
      <ul class="pagination">
    <li class="page-item"><?php echo $links; ?></li>
  </ul>
</nav>
</body>

</html>