<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Codeigniter CRUD Application With Example - Tutsmake.com</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">
    <style>
        .mt40 {
            margin-top: 40px;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="pull-left">
                <h2>Livres</h2>
            </div>
            <div class=" mt40 centered">

                <div class="card" style="width: 20rem; margin:auto">
                    <img class="card-img-top" src="http://placekitten.com/g/300/300" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title"><?php echo $book->Titre ?></h5>
                        <p class="card-text"><?php echo "De" . " " . $book->Nom . " " . $book->Prenom ?></p>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><?php echo $book->Format_livre ?></li>
                        <li class="list-group-item"><?php echo $book->Section ?></li>
                        <li class="list-group-item"><?php echo $book->Type ?></li>
                    </ul>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <a href="<?php echo base_url('book/edit/' . $book->id) ?>" class="btn btn-primary">Modifier</a>
                            </div>
                            <div class="col-md-6">
                                <form action="<?php echo base_url('book/delete/' . $book->id) ?>" method="post">
                                    <button class="btn btn-danger" type="submit">Supprimer</button>
                                </form>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>