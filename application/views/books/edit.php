<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Codeigniter CRUD Application With Example - Tutsmake.com</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">
    <style>
        .mt40 {
            margin-top: 40px;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 mt40">
                <div class="pull-left">
                    <h2>Edit Note</h2>
                </div>
            </div>
        </div>
        <form action="<?php echo base_url('book/store') ?>" method="POST" name="edit_book">
            <input type="hidden" name="id" value="<?php echo $book->id ?>">
            <div class="row">
                <?php foreach ($fieldBook as $field => $label) : ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <strong><?php echo $label; ?></strong>

                            <input type="text" name="<?php echo $field; ?>" class="form-control" value="
                        <?php switch ($label) {
                            case 'Titre':
                                echo $book->Titre;
                                break;
                            case 'Nom':
                                echo $book->Nom;
                                break;
                            case 'Prenom':
                                echo $book->Prenom;
                                break;
                            case 'Editeur':
                                echo $book->Editeur;
                                break;
                            case 'Format':
                                echo $book->Format_livre;
                                break;
                            case 'Type':
                                echo $book->Type;
                                break;
                            case 'Section':
                                echo $book->Section;
                                break;
                            case 'Etagere':
                                echo $book->Etagere;
                                break;
                            case 'Rangees':
                                echo $book->Rangee;
                                break;
                        }; ?>" placeholder="Entrez <?php echo $label; ?>">
                        </div>
                    </div>
                <?php endforeach; ?>
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
    </div>
</body>

</html>