<?php
class Book_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    // db rows total
    public function get_count()
    {
        return $this->db->count_all('dataset');
    }

    // return all data by search using pagination library
    function search($keyword, $limit, $start)
    {
        $this->db->limit($limit, $start);
        $this->db->like('Titre', $keyword);
        $query  =  $this->db->get('dataset');
        return $query->result();
    }

    public function get_book_by_id($id)
    {
        $query = $this->db->get_where('dataset', array('id' => $id));
        return $query->row();
    }

    public function createOrUpdate()
    {
        $this->load->helper('url');
        $id = $this->input->post('id');

        $data = array(
            'Titre' => $this->input->post('title'),
            'Nom' => $this->input->post('name'),
            'Prenom' => $this->input->post('firstname'),
            'Editeur' => $this->input->post('editor'),
            'Format_livre' => $this->input->post('book_format'),
            'Type' => $this->input->post('type'),
            'Section' => $this->input->post('section'),
            'Etagere' => $this->input->post('shelf'),
            'Rangee' => $this->input->post('row')
        );
        if (empty($id)) {
            return $this->db->insert('dataset', $data);
        } else {
            $this->db->where('id', $id);
            return $this->db->update('dataset', $data);
        }
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('dataset');
    }
}
